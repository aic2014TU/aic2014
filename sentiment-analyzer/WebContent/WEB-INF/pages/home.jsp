<%@ include file="templates/header.jsp"%>

<div class="container" ng-controller="aCtrl">
<div  style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"> 
	  <div class="form-group">
          <label for="query"  class="col-md-3 control-label">Query</label>
              <div class="col-md-9">
               <input type="text" ng-model="query" required class="form-control" name="query" placeholder="Query">
       		</div>
       </div>                       
                                    
       <div class="form-group">
		  	  <label for="sel1">Classifier</label>
			  <select ng-model="classifier"   id="sel1">
			  <option selected value="LibSVM">SVM</option>
			  <option value="NaiveBayes" >Naive Bayes</option>
			  <option value="RandomForest">Random Forest</option>
			 </select>
		</div>
								
		<div class="form-group" >
			  <label for="sel2">Training Data</label>
			  <select ng-model="data"  id="sel2">
			  <option selected value="50">50</option>
			  <option value="1000" >1000</option>
			  <option value="2000">2000</option>
			  </select>
		</div>
								
        <div class="form-group">
             <!-- Button -->                                        
             <div class="col-md-offset-3 col-md-9">
                 <a ng-click=analyze()
                      type="button" class="btn btn-info">  Search</a>
             </div>
        </div>
     						  
     	  <div>
     			<label >{{result.message}}</label>
     	  </div>
      </div>
</div>

