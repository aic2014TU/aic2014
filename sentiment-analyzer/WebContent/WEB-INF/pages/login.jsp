   <%@ include file="templates/header.jsp" %>
    <div  class="container" >    
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title">Login</div>
                    </div>     

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                          
                           <c:if test="${not empty error}">
								<div style="color: #ff0000; class="error">${error}</div>
							</c:if>
							
                        <form  id="loginform" action="<c:url value='j_spring_security_check' />" method='POST' class="form-horizontal" role="form">
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="j_username" type="text" class="form-control" name='j_username' value="" placeholder="username">                                        
                                    </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="j_password" type="password" class="form-control" name='j_password' placeholder="password">
                                    </div>
                                    

                                
                           


                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                      <button class="btn btn-large btn-primary" type="submit">Login</button>

                                    </div>
                                </div>


                                 
                            </form>     



                        </div>                     
                    </div>  
        </div>
        
    </div>
