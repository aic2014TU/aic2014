<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html ng-app="sentimentApp">
<head>
<meta charset="utf-8">
<title> Welcome</title>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css"
	href="resources/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="resources/css/navbar.css" />

<script type="text/javascript" src="resources/script/jquery.min.js"></script>
<script type="text/javascript" src="resources/script/bootstrap.min.js"></script>
<script type="text/javascript"
	src="resources/script/angular-1.3.2/angular.min.js"></script>
<script type="text/javascript"
	src="resources/script/angular-1.3.2/angular-route.min.js"></script>
<script type="text/javascript"
	src="resources/script/angular-1.3.2/angular-resource.min.js"></script>

<script type="text/javascript"
	src="resources/script/angular-1.3.2/angular-sanitize.min.js"></script>

<script type="text/javascript" src="resources/app/js/controllers.js"></script>
<script type="text/javascript" src="resources/app/js/userController.js"></script>



</head>
<body>
	<c:url value="/j_spring_security_logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>


	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/sentiment-analyzer/home.html">Home</a>
			</div>
			<div class="navbar-collapse collapse" >
				<ul class="nav ">
					<ul class="nav navbar-nav ">
						
						
						<security:authorize access="!isAuthenticated()">
							<li><a href="/sentiment-analyzer/login.html">Login</a></li>
							<li><a href="/sentiment-analyzer/signup.html">Sign up</a></li>

						</security:authorize>

						<security:authorize access="isAuthenticated()">
							<li >
								<div style="margin-top:4px" >
									<div ng-controller=loginCtrl class="dropdown">
									  <button   class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
									   <span class="glyphicon glyphicon-user" aria-hidden="true"></span> {{user.username}}
									    <span class="caret"></span>
									  </button>
									  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
									    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:formSubmit()">Logout</a></li>
									  </ul>
									</div>
								</div>
							</li>
						</security:authorize>
					</ul>
					

					</ul>
			</div>
		</div>
	</div>
