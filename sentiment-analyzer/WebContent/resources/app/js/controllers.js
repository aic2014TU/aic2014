var sentimentApp = angular.module('sentimentApp',[ 'ngSanitize','ngRoute','ngResource']);



  
sentimentApp.controller('aCtrl', function ($scope, $http) {
	$scope.classifier = 'LibSVM';
	$scope.data = '50';
	$scope.analyze = function(){
		$http.get('analyze?query='+$scope.query+'&classifier='+$scope.classifier+'&data='+$scope.data).
		  success(function(data, status, headers, config) {
		    $scope.result = data;
		  }).
		  error(function(data, status, headers, config) {
		    alert(status);
		  });
	};
	
	
});
 



