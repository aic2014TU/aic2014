sentimentApp.factory("User", function($resource) {
	  return $resource("users/:id");
	});




sentimentApp.controller("signupCtrl", function($scope, User,$http) {
	$scope.refresh = function(){
		$scope.user = new User();
		
	}
	
	
	
	$scope.loadDataById=function(data){
		$scope.user = User.get({id:data});
	};
	$scope.register = function(){
		var res = $http.post("register",$scope.user).
		  success(function(data, status, headers, config) {
			  	if(status==200){
			  		window.location = "login";
			  	} else {
			  		$scope.error  = data;
			  		$scope.refresh();
			  	}
			    
			  }).
			  error(function(data, status, headers, config) {
			    $scope.error = data;
			  });
	}
	
	
	$scope.cancel=function(){
		window.location="home";
	}
});

sentimentApp.controller('loginCtrl', function ($scope, $http) {
	  $http.get("username"). 
	  success(function(data, status, headers, config) {
		  	$scope.user=data;
	  }).
	  error(function(data, status, headers, config) {
	  });
});
