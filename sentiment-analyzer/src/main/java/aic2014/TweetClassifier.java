package aic2014;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import twitter4j.Status;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.trees.RandomForest;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;
import weka.core.converters.CSVLoader;


/**
 * 
 * Simple slow classifier. Modification  from tutorial at  
 * http://preciselyconcise.com/apis_and_installations/training_a_weka_classifier_in_java.php
 * Dataset of classified tweets for training in training is too small.
 * Tried big dataset with more than  1.5 million tweets, but it takes too long to build classifier more than 24h
 * on my laptop, so i didn't succeed 
 * Had problems with adding LibSVM classifier so added LibSVM class to src, if it works without, please delete
 * Sorry for bad code(
 *
 */
public class TweetClassifier {
	
	private ArrayList<String> featureWords;
	private Classifier classifier;
    private ArrayList<String> sentimentClassList;
    private ArrayList<Attribute> attributeList;
	private Instances inputDataset;
	
	/*
	 * LibSVM50,1000,2000 - SVM with training data 50,1000,2000
	 * NaiveBayes50,1000,2000 - NaiveBayes classifier with training data 50,1000,2000
	 * RandomForest50,1000,2000 -RandomForest classifier with training data 50,1000,2000
	 */
	private String classifierName = "LibSVM1000";
	
	
	
	public void setClassifierName(String classifierName) {
		this.classifierName = classifierName;
	}

	public  TweetClassifier() {
		try {
			initialize();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void createFeatureVector(){ 
		ObjectInputStream ois = null;
		InputStream is = null;
		BufferedReader in = null;
		featureWords = new ArrayList<>();
        try {
        	//ois = new ObjectInputStream(new FileInputStream("files/FeatureWordsList.dat"));
        	ois = new ObjectInputStream(this.getClass().getClassLoader().getResourceAsStream("files/FeatureWordsList.dat"));
            featureWords = (ArrayList<String>) ois.readObject();
            
        } catch (Exception ex) {
        	ex.printStackTrace();
            System.out.println("Exception in Deserialization");
        } finally {
            try {
            	 ois.close();
            } catch (IOException ex) {
                System.out.println("Exception while closing file after Deserialization");
            }
        }
		
	}
	 
	// creates attributes from feature vector
	public  void initialize() throws IOException
	{
		 sentimentClassList = new ArrayList<>();
	     sentimentClassList.add("positive");
	     sentimentClassList.add("negative");
	     
	     attributeList = new ArrayList<>();    
	     
	     
	     createFeatureVector();
		 for(String featureWord : featureWords)
         {
            attributeList.add(new Attribute(featureWord));
           
         }
		
        //the last attribute reprsents ths CLASS (Sentiment) of the tweet
        attributeList.add(new Attribute("Sentiment",sentimentClassList));   
        
		
	}
	
	
	
	
	private Instances createInstances(final String INSTANCES_NAME)
    {
        
		
        //create an Instances object with initial capacity as zero 
        Instances instances = new Instances(INSTANCES_NAME,attributeList,0);
        
        //sets the class index as the last attribute (positive or negative)
        instances.setClassIndex(instances.numAttributes()-1);
            
        return instances;
    }
	
	
	
	private Instance extractFeature(Instance inputInstance) throws IOException
    {
		
        Map<Integer,Double> featureMap = new HashMap<>();
        TweetManager tm = new TweetManager();
        String processedTweet = tm.processTweet(inputInstance.stringValue(1));
        
        for(int i=0;i<featureWords.size();i++){
        	featureMap.put(i,0.0);
        }
      
        for(String word : processedTweet.split((" "))){
        	if(featureWords.contains(word))
            {
                //adding 1.0 to the featureMap represents that the feature word is present in the input data
               featureMap.put(featureWords.indexOf(word),1.0);
            }
        }
       
        int indices[] = new int[featureMap.size()+1];
        double values[] = new double[featureMap.size()+1];
        int i=0;
        
        for(Map.Entry<Integer,Double> entry : featureMap.entrySet())
        {
            indices[i] = entry.getKey();
            values[i] = entry.getValue();
            i++;
        }
        
        indices[i] = featureWords.size();
        // sentiment of the tweet
        values[i] = (double)sentimentClassList.indexOf(inputInstance.stringValue(0));

        return new SparseInstance(1.0,values,indices,featureWords.size());
    }
	
	// same as extractFeature but for String and sentiment of the tweet is not given
	private Instance  extractFeatureOfTweet(String tweet) throws IOException {
		Map<Integer,Double> featureMap = new TreeMap<>();
        TweetManager tm = new TweetManager();
        String processedTweet = tm.processTweet(tweet);
        
        // initialize map with zeros
        for(String word:featureWords){
        	featureMap.put(featureWords.indexOf(word),0.0);
        }
        
        for(String word : processedTweet.split((" "))){
        	if(featureWords.contains(word))
            {
                //adding 1.0 to the featureMap represents that the feature word is present in the input data
               featureMap.put(featureWords.indexOf(word),1.0);
            }
        }
        
       
        int indices[] = new int[featureMap.size()+1];
        double values[] = new double[featureMap.size()+1];
        int i=0;
      
        
        for(Map.Entry<Integer,Double> entry : featureMap.entrySet())
        {
            indices[i] = entry.getKey();
            values[i] = entry.getValue();
            i++;
        }
        
        indices[i] = featureWords.size();
        //  Sentiment 
        values[i] = 0;
        return new SparseInstance(1.0,values,indices,featureWords.size());
		
		
	}
	
	// Load csv file
	private void getTrainingDataset(final String INPUT_FILENAME)
    {
        try{
            //reading the training dataset from CSV file
            CSVLoader trainingLoader =new CSVLoader();
            trainingLoader.setSource(new File(INPUT_FILENAME));
            inputDataset = trainingLoader.getDataSet();
        }catch(IOException ex)
        {
        	ex.printStackTrace();
            System.out.println("Exception in getTrainingDataset Method");
        }
    }
	
	
	public void trainClassifier(final String INPUT_FILENAME) throws IOException
    {
		
			getTrainingDataset(INPUT_FILENAME);
            
			//trainingInstances consists of feature vector of every input
			Instances trainingInstances = createInstances("TRAINING_INSTANCES");
            
            for(Instance currentInstance : inputDataset)
            {
                //extractFeature method returns the feature vector for the current input
                Instance currentFeatureVector = extractFeature(currentInstance);
                
                //Make the currentFeatureVector to be added to the trainingInstances
                currentFeatureVector.setDataset(trainingInstances);
                trainingInstances.add(currentFeatureVector);
            }
            
        
            classifier = new  LibSVM();
            try {
            	//classifier training code
            	classifier.buildClassifier(trainingInstances);
            	
            	//storing the trained classifier to a file for future use
            	weka.core.SerializationHelper.write("files/LibSVM2000.model",classifier);
            } catch (Exception ex) {
            	ex.printStackTrace();
            	System.out.println("Exception in training the classifier.");
            }
    }
	
	
	public ClassifiedTweet classifyTweet(String tweet) throws IOException{
		List<String> tweets = new ArrayList<>();
		tweets.add(tweet);
		return(classifyTweets(tweets).get(0));
	}
	
	public List<ClassifiedTweet> classifyTweets(List<String> tweets) throws IOException{
		List<ClassifiedTweet> results = new ArrayList<>();
		Instances classifiyingInstances = createInstances("CLASIFIYING_INSTANCES");
		List<Double> sentimentValues =new ArrayList<>();
		
		for(String tweet : tweets){
		    Instance instance = extractFeatureOfTweet(tweet);
		    instance.setDataset(classifiyingInstances);
		    classifiyingInstances.add(instance);
		}
		
	    try {
			classifier = (Classifier) weka.core.SerializationHelper.read("files/"+classifierName+".model");
	    	//classifier = (Classifier) weka.core.SerializationHelper.read(
	    	//		this.getClass().getClassLoader().getResourceAsStream("files/"+classifierName+".model"));
			for(Instance testInstance : classifiyingInstances)
	            {
					Double score = classifier.classifyInstance(testInstance);
					if(score.equals(0.0))
						sentimentValues.add(1.0);
					else
						sentimentValues.add(-1.0);
	                //System.out.println(" score="+(1-score));
	               // System.out.println(trainingInstances.attribute("Sentiment").value((int)score));
	            }
			for(int i=0; i<tweets.size();i++){
				results.add(new ClassifiedTweet(tweets.get(i),sentimentValues.get(i)));
			}
			return results;
			
		} catch (Exception e) {
			System.out.println("Exception in classifying method");
			return null;
		}
	}
	
	public List<ClassifiedTweetWithStatus> classifyTweetsWithStatus(List<Status> tweets) throws IOException{
		List<ClassifiedTweetWithStatus> results = new ArrayList<>();
		Instances classifiyingInstances = createInstances("CLASIFIYING_INSTANCES");
		List<Double> sentimentValues =new ArrayList<>();
		
		for(Status tweet : tweets){
		    Instance instance = extractFeatureOfTweet(tweet.getText());
		    instance.setDataset(classifiyingInstances);
		    classifiyingInstances.add(instance);
		}
		
	    try {
			//classifier = (Classifier) weka.core.SerializationHelper.read("files/LibSVM.model");
	    	classifier = (Classifier) weka.core.SerializationHelper.read(
	    			this.getClass().getClassLoader().getResourceAsStream("files/"+classifierName+".model"));
			for(Instance testInstance : classifiyingInstances)
	            {
					Double score = classifier.classifyInstance(testInstance);
					// score = 0.0 - positive, score =1.0 -negative
					if(score.equals(0.0))
						sentimentValues.add(1.0);
					else
						sentimentValues.add(-1.0);
	            }
			
			for(int i=0; i<tweets.size();i++){
				results.add(new ClassifiedTweetWithStatus(sentimentValues.get(i),tweets.get(i)));
			}
			
			return results;
			
		} catch (Exception e) {
			System.out.println("Exception in classifying method");
			return null;
		}
	}
	
	public void testClassifier(final String INPUT_FILENAME) throws IOException
    {
		List<String> sentiments = new ArrayList<>();
        getTrainingDataset(INPUT_FILENAME);
            
        //trainingInstances consists of feature vector of every input
        Instances testingInstances = createInstances("TESTING_INSTANCES");

        for(Instance currentInstance : inputDataset)
        {
            //extractFeature method returns the feature vector for the current input
            Instance currentFeatureVector = extractFeature(currentInstance);

            //Make the currentFeatureVector to be added to the trainingInstances
            currentFeatureVector.setDataset(testingInstances);
            testingInstances.add(currentFeatureVector);
        }
            
            
        try {
            //Classifier deserialization
            classifier = (Classifier) weka.core.SerializationHelper.read("files/LibSVM.model");
            
            //classifier testing code
            for(Instance testInstance : testingInstances)
            {
                double score = classifier.classifyInstance(testInstance);
                sentiments.add(testingInstances.attribute("Sentiment").value((int)score));
            }
            
            int errorsNumber =0;
            for(int i = 0; i<inputDataset.size();i++){
            	if(!inputDataset.get(i).stringValue(0).equals(sentiments.get(i)))
            			errorsNumber++;
            }
            System.out.println("Effectiveness is:"+(double)(inputDataset.size()-errorsNumber)/inputDataset.size()+"%");
        } catch (Exception ex) {
        	ex.printStackTrace();
            System.out.println("Exception in testing the classifier.");
        }
    }
	
	public static void main(String[] args){
		TweetClassifier fv = new TweetClassifier();
		
		try {
			TweetManager tm = new TweetManager();
			//fv.trainClassifier("files/trainingSet1000.csv");
			fv.setClassifierName("LibSVM2000");
			System.out.println("trained");
			List<ClassifiedTweetWithStatus> clTweets = fv.classifyTweetsWithStatus(tm.getTweetsWithStatus("sad",1));
			for(ClassifiedTweetWithStatus t : clTweets){
				System.out.println(t.getSentiment()+", "+t.getTweet().getText());
			}
			System.out.println(SentimentAggregator.agregateTweetsWithWeigth(clTweets));
			//fv.testClassifier("files/testing.csv");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
