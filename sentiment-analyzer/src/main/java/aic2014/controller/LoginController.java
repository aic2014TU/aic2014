package aic2014.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import aic2014.dao.UserDao;


@Controller
public class LoginController {

	@Autowired
	UserDao uDao;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout) {
 
	  ModelAndView model = new ModelAndView("login");
	  if (error != null) {
		model.addObject("error", "Invalid username or password!");
	  }
 
	  if (logout != null) {
		model.addObject("msg", "You've been logged out successfully.");
	  }
	  
 
	  return model;
 
	}
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView signup() {
 
	  ModelAndView model = new ModelAndView("signup");
	  
	  
		
		 
		
		
		
 
	  return model;
 
	}
	
	@RequestMapping(value="/username", method = RequestMethod.GET)
	@ResponseBody
	public aic2014.model.User getUsername(){
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		aic2014.model.User newUser = new aic2014.model.User();
		newUser.setUsername(user.getUsername());
	    return newUser; //get logged in username
	}
	
	
	
	//for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {
 
	  ModelAndView model = new ModelAndView();
 
	  //check if user is login
	  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	  if (!(auth instanceof AnonymousAuthenticationToken)) {
		UserDetails userDetail = (UserDetails) auth.getPrincipal();	
		model.addObject("username", userDetail.getUsername());
	  }
 
	  model.setViewName("403");
	  return model;
 
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST,consumes = "application/json")
	@ResponseBody
	public Integer saveUser(@RequestBody aic2014.model.User user){
		aic2014.model.User newUser = new aic2014.model.User();
		newUser.setPassword(user.getPassword());
		newUser.setUsername(user.getUsername());
		newUser.setEmail(user.getEmail());
		newUser.setEnable(1);
		newUser.setRole("user");
		uDao.save(newUser);
		return newUser.getId();
	}
}
