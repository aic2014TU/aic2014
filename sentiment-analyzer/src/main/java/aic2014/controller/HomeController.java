package aic2014.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import aic2014.MessageDTO;
import aic2014.service.AnalyzeService;

@Controller
public class HomeController {

	@Autowired
	AnalyzeService aService;
	
	/**
	 *  controller for home page
	 */
	@RequestMapping( "/home" )
	public ModelAndView home(){
		 return new ModelAndView("home");
	}
	
	@RequestMapping(value="/analyze",method = RequestMethod.GET )
	@ResponseBody
	public MessageDTO analyze(@RequestParam(value="query")String query,
			@RequestParam(value="classifier")String classifier,
			@RequestParam(value="data")String data){
		MessageDTO message = new MessageDTO();
		message.setMessage(aService.analyze(query, classifier, data));
		message.setSuccess(true);
		return message;
	}
}
