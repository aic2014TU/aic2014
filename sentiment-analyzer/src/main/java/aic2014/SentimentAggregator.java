package aic2014;

import java.util.Date;
import java.util.List;

import twitter4j.GeoLocation;

/**
 * Simple aggregator of tweets.
 * aggregation with weigths :
 *  1) if tweet was posted by verified account it has higher value by 0.1;
 *  2) if tweet was posted somewhere near 100 km from Vienna it has higher value by 0.1;
 *  3) if tweet was posted today it has higher value by 0.1;
 */
public class SentimentAggregator {
  public static void agregateTweets(List<ClassifiedTweet> tweets){
	  int positive=0, negative=0;
	  double sum=0;
	  for(ClassifiedTweet tweet: tweets){
		  if(tweet.getSentiment() < 0)
			  negative++;
		  else 
			  positive++;
		  sum+=tweet.getSentiment();	  
	  }
	  System.out.println("Number of postivie tweets: "+positive);
	  System.out.println("Number of negative tweets: "+negative);
	  if(sum>0)
		  System.out.println("Overall sentiment is positive ");
	  else
		  System.out.println("Overall sentiment is negative");
  }
  
  public static String agregateTweetsWithWeigth(List<ClassifiedTweetWithStatus> tweets){
	  String result = "";
	  tweets = assignWeights(tweets);
	  int positive=0, negative=0;
	  double sum=0;
	  for(ClassifiedTweetWithStatus tweet: tweets){
		  if(tweet.getSentiment() < 0)
			  negative++;
		  else 
			  positive++;
		  sum+=tweet.getSentiment()*tweet.getWeight();	  
	  }
	  result += "Number of postivie tweets: "+positive+" \n";
	  result+="\n";
	  result +="Number of negative tweets: "+negative+" \n";
	  result +="\n";
	  if(sum>0)
		  result +="Overall sentiment is positive ";
	  else
		  result +="Overall sentiment is negative";
	  return result;
  }
  
  private static List<ClassifiedTweetWithStatus> assignWeights(List<ClassifiedTweetWithStatus> tweets){
	  for(ClassifiedTweetWithStatus tweet : tweets){
		  double weight = 0.1;
		  
		  // if user is verified then increase weight by 0.1
		  if(tweet.getTweet().getUser() != null){
			  if( tweet.getTweet().getUser().isVerified()){
				  weight+=0.1;
			  }
		  }
		  
		  /* if geo location of tweet is less than 100 km  from Vienna increase by 0.1
		   * Coordinates of Vienna in decimal degrees
		   * Latitude: 48.2084900	
		   * Longitude: 16.3720800
		  */
		  if(tweet.getTweet().getGeoLocation() != null){
			  GeoLocation location = tweet.getTweet().getGeoLocation();
			  double distance = getDistance(location.getLatitude(),location.getLongitude(),48.2084900,16.3720800);
			  if( distance < 100.0){
				  weight += 0.1;
			  }
		  }
		  
		  
		  // if tweet is from today
		  Date dateOfTweet = tweet.getTweet().getCreatedAt();
		  Date dateCurrent = new Date();
		  long diffDays = (dateCurrent.getTime() - dateOfTweet.getTime()) / (24 * 60 * 60 * 1000);
		  if(diffDays == 0){
			  weight+=1;
		  }
		  tweet.setWeight(weight);
	  }
	  return tweets;
  }
  
  private static double getDistance(double lat1, double lon1, double lat2, double lon2) {
	    double theta = lon1 - lon2;
	    double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
	    dist = Math.acos(dist);
	    dist = rad2deg(dist);
	    dist = dist * 60 * 1.1515;
	    dist = dist * 1.609344;
	    return (dist);
	  }
	   
	  /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	  /*::  This function converts decimal degrees to radians             :*/
	  /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
  private static double deg2rad(double deg) {
	    return (deg * Math.PI / 180.0);
  }
	   
	  /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	  /*::  This function converts radians to decimal degrees             :*/
	  /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
   private static double rad2deg(double rad) {
	    return (rad * 180 / Math.PI);
  }

}
