package aic2014.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import aic2014.ClassifiedTweetWithStatus;
import aic2014.SentimentAggregator;
import aic2014.TweetClassifier;
import aic2014.TweetManager;
import aic2014.service.AnalyzeService;;

@Service
public class AnalyzeServiceImpl implements AnalyzeService {

	@Override
	public String analyze(String query, String classifier, String data) {
		TweetClassifier fv = new TweetClassifier();
		
		try {
			TweetManager tm = new TweetManager();
			
			//choose model according classifier and training data , no check for bad input
			fv.setClassifierName(classifier+data);
			
			
			List<ClassifiedTweetWithStatus> clTweets = fv.classifyTweetsWithStatus(tm.getTweetsWithStatus(query,1));
			for(ClassifiedTweetWithStatus t : clTweets){
				System.out.println(t.getSentiment()+", "+t.getTweet().getText());
			}
			String result = SentimentAggregator.agregateTweetsWithWeigth(clTweets);
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}
