package aic2014.service;

public interface AnalyzeService {

	public String analyze (String query,String classifier,String data);
}
