package aic2014;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
 
public class NLP {
    static StanfordCoreNLP pipeline;
    
    public NLP() {
    	pipeline = new StanfordCoreNLP("MyPropFile.properties");
    }
 
    public String findSentiment(String tweet) {
 
    	String mainSentiment = "";
        if (tweet != null && tweet.length() > 0) {
            int longest = 0;
            Annotation annotation = pipeline.process(tweet);
            String[] sentimentText = { "Very Negative","Negative", "Neutral", "Positive", "Very Positive"};
            for (CoreMap sentence : annotation
                    .get(CoreAnnotations.SentencesAnnotation.class)) {
            	Tree tree = sentence.get(SentimentCoreAnnotations.AnnotatedTree.class);
                int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
                String partText = sentence.toString();
                if (partText.length() > longest) {
                    mainSentiment = sentimentText[sentiment];
                    longest = partText.length();
                }
            }
        }
        return mainSentiment;
    }
}