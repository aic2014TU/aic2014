package aic2014.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import aic2014.model.User;



@Repository
@Transactional
public interface UserDao extends JpaRepository<User, Integer>{

}
